from django.db import models

# Create your models here.
class PhoneNumber(models.Model):
    number = models.CharField(max_length =20,null=False)

class Customer(models.Model):
    age = models.IntegerField(default=0)
    address = models.CharField(max_length =100)
    gender = models.CharField(max_length =20)
    img = models.ImageField(upload_to='images/',null=True)
    number = models.ForeignKey("PhoneNumber", on_delete=models.CASCADE)



class Escort(models.Model):
    number = models.ForeignKey("PhoneNumber", on_delete=models.CASCADE)
    age = models.IntegerField(default=0)
    address = models.CharField(max_length =100)
    gender = models.CharField(max_length =20)
    img = models.ImageField(upload_to='images/',null=True)

class TwilloNumber(models.Model):
    number = models.CharField(max_length =20,null=False)


class About(models.Model):
    p1_tag = models.TextField()
    p2_tag = models.TextField()
    p3_tag = models.TextField()


class Title(models.Model):
    t1_tag = models.TextField()
    t2_tag = models.TextField()
    