from django.apps import AppConfig


class TwilloManagerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'twillo_manager'
