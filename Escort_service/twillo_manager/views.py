from django.shortcuts import render
from .models import Customer,Escort,TwilloNumber
# Create your views here.
from django.views.decorators.csrf import csrf_exempt
from twilio.twiml.messaging_response import MessagingResponse

def index(request):
    number = TwilloNumber.objects.first()
    return render(request,'index.html',{'number':number})


def model_profile(request):
    return render(request,'model-profile.html')

def make_payment(request):
    return render(request,'make-payment.html')

def payment(request):
    return render(request,'payment.html')

def chat(request):
    return render(request,'chat.html')

def save_user_data(request):
    number = request.POST.get('phone')
    age = request.POST.get('age')
    role = request.POST.get('role')
    gender = request.POST.get('gender')
    address =  request.POST.get('city')
    file_value = request.POST.get('file')

    if not number : 
        raise Exception("Numebr is invalid")

    if role:
        if role == "customer":
            cust_obj = Customer.objects.create(number=number,age=age,gender=gender,address=address,img=file_value)
            cust_obj.save()
        elif role == "escort":
            escort_obj = Escort.objects.create(number=number,age=age,gender=gender,address=address,img=file_value)
            escort_obj.save()
    return render(request,'index.html')

    
@csrf_exempt
def message(request):
    user = request.POST.get('From')
    message = request.POST.get ('Body')
    print(f'{user} says {message}')
    
    response = MessagingResponse()
    response.message('what is your role either male or female')
    return HttpResponse(str(response))