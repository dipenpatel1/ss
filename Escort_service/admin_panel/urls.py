"""Escort_service URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path,include
from . import views
from twillo_manager.views import save_user_data
urlpatterns = [

    path('', views.index),
    path('index2/', views.index2),
    path('index3/', views.index3),
    path('starter/', views.starter),
    path('iframe/', views.iframe),
    path('register/', views.registration, name="register-customer"),
    path('save_data/', views.save_register_data, name="save_data"),
    path('get_all_customer/', views.get_all_customer, name="get_all_customer"),
    path('update_content/', views.update_about_content, name="update-about-content"), 
    path('submit_about_data/', views.submit_about_data, name="submit_about_data"), 
    path('submit_title_data/', views.submit_title_data, name="submit_title_data"),
    path('update_title_content/', views.update_title_content, name="update_title_content"),

    
    ]
    


