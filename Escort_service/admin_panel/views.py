from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.
from twillo_manager.models import Customer,Escort,About,Title

def index(request):
    return render(request,'admin_panel/index.html')

def index2(request):
    return render(request,'admin_panel/index2.html')

def index3(request):
    return render(request,'admin_panel/index3.html')

def starter(request):
    return render(request,'admin_panel/starter.html')

def iframe(request):
    return render(request,'admin_panel/iframe.html')

def iframe_dark(request):
    return render(request,'admin_panel/iframe-dark.html')


def registration(request):
    return render(request,'admin_panel/registration.html')



def save_register_data(request):
    number = request.POST.get('phone')
    age = request.POST.get('age')
    role = request.POST.get('role')
    gender = request.POST.get('gender')
    address =  request.POST.get('city')
    file_value = request.POST.get('file')

    if not number : 
        raise Exception("Numebr is invalid")

    if role:
        if role == "customer":
            cust_obj = Customer.objects.create(number=number,age=age,gender=gender,address=address,img=file_value)
            cust_obj.save()
        elif role == "escort":
            escort_obj = Escort.objects.create(number=number,age=age,gender=gender,address=address,img=file_value)
            escort_obj.save()
    return render(request,'admin_panel/index.html')

def get_all_customer(request):
    customer_data = Customer.objects.all()
    return render(request,'admin_panel/data_table.html',{'customers':customer_data})


def update_about_content(request):
    return render(request,'admin_panel/update_about_content.html')

def submit_about_data(request):
    p1_tag = request.POST.get('p1_tag')
    p2_tag = request.POST.get('p2_tag')
    p3_tag = request.POST.get('p3_tag')
    obj = About.objects.filter(id = 1).first()
    if not obj:
        obj = About.objects.create()
        

    if p1_tag:
        obj.p1_tag = p1_tag
        
    if p2_tag:
        obj.p2_tag = p2_tag
        
    if p3_tag:
        obj.p3_tag = p3_tag
    obj.save() 

    return render(request,'admin_panel/index.html')

def update_title_content(request):
    return render(request,'admin_panel/update_title_content.html')


def submit_title_data(request):
    t1_tag = request.POST.get('t1_tag')
    t2_tag = request.POST.get('t2_tag')
    obj = Title.objects.filter(id = 1).first()
    if not obj:
        obj = Title.objects.create()

    if t1_tag:
        obj.t1_tag = t1_tag
        
    if t2_tag:
        obj.t2_tag = t2_tag
    obj.save() 

    return render(request,'admin_panel/index.html')